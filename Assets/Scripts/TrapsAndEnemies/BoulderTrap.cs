﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderTrap : MonoBehaviour
{
    

    public Vector2 startPos;
    public Vector2 startShadowScale;


    Rigidbody2D rigidbody;
    float scaleX = 1f;
    bool growing = true;


    public bool tochedPit = false;


    public BoulderParent boulderParent;
    public Transform shadowTransform;
    [SerializeField] float timeBetweenGrow = 0.1f;

    bool grounded = false;
    

    
    private void Start()
    {
        startPos = transform.position;
        startShadowScale = shadowTransform.localScale;

        rigidbody = GetComponent<Rigidbody2D>();
        StartCoroutine(scaleGrow());
        rigidbody.gravityScale = 0;
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BerserkController berserkController = collision.GetComponent<BerserkController>();

        if (berserkController)
        {
            berserkController.Die();
        }

        if (collision.GetComponent<Ground>()&&tochedPit == false)
        {
            //rigidbody.simulated = false;
            //rigidbody.gravityScale = 0;
            rigidbody.constraints = RigidbodyConstraints2D.FreezePositionY;
            boulderParent.RestartTrap();
            grounded = true;
            boulderParent.Main.cameraController.BoulderShake();
            
        }


        if (collision.GetComponent<Pit>()) 
        {
            tochedPit = true;
            
            boulderParent.RestartTrap();
            grounded = true;
        }

    }

    void StartFall() 
    {
        rigidbody.gravityScale = 3;
    }

    IEnumerator scaleGrow() 
    {
        yield return new WaitForSeconds(timeBetweenGrow);
        scaleX = scaleX + 0.1f;
        shadowTransform.localScale = new Vector2(scaleX, 0.3f);
        if (growing) 
        {
            if (scaleX >= 3f) 
            {
                growing = false;
                StartFall();
            }
            StartCoroutine(scaleGrow());
        }

    }
   
}
