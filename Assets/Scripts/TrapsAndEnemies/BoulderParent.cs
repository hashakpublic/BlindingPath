﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderParent : TrapParent
{
    public BoulderTrap boulderTrap;
    private void Start()
    {
        base.Start();
        boulderTrap.boulderParent = this;
    }
    public void RestartTrap() 
    {
        interactable = false;
        transform.parent = backGroundMovement.transform;
        //StartCoroutine(disable());
    }

    IEnumerator disable()
    {
        yield return new WaitForSeconds(3f);
        ready = true;
        transform.parent = null;
        boulderTrap.transform.position = boulderTrap.startPos;
        boulderTrap.shadowTransform.localScale = boulderTrap.startShadowScale;
        gameObject.SetActive(false);
    }
}
