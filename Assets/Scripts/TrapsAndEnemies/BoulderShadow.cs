using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderShadow : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    public BoulderTrap trap;
    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();    
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<BaseTrap>())
        {
            spriteRenderer.enabled = false;
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<BaseTrap>()&&trap.tochedPit == false)
        {
            spriteRenderer.enabled = true;
        }
    }

}
