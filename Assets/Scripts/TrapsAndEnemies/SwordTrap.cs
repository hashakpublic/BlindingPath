﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordTrap : TrapParent 
{
    bool aiming = false;
    
    [SerializeField] float delayBeforeShoot = 2;
    [SerializeField] float force = 100;
    bool grounded = false;
    bool shooted = false;
    Animator animator;
    BerserkController berserk;
    

    SpriteRenderer spriteRenderer;
    void Start()
    {
        base.Start();
        AssignRigidbody();
        berserk = BerserksTransform.GetComponent<BerserkController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
        StartCoroutine(fixAnim());
        rigidbody2D.simulated = false;
        animator = GetComponent<Animator>();        
        StartCoroutine(aimAndShoot());
        
    }
  
    private void FixedUpdate()
    {
        if ((shooted == false)&&(berserk!=null))
        {
            var angle = Vector2.Angle(Vector2.right, berserk.transform.position - transform.position);
            transform.eulerAngles = new Vector3(0f, 0f, transform.position.y < berserk.transform.position.y ? angle - 90 : -angle - 90);
        }

       
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BerserkController berserkController = collision.GetComponent<BerserkController>();

        if (berserkController)
        {
            berserkController.Die();
        }

        if (collision.GetComponent<Ground>())
        {
            rigidbody2D.simulated = false;
            //StartCoroutine(disable());
            grounded = true;
            //transform.parent = backGroundMovement.transform;
            interactable = false;

            transform.parent = backGroundMovement.transform;
            //rigidbody2D.velocity = new Vector2(axeTrap.backGroundMovement.speed, 0);
            
        }

    }

    IEnumerator aimAndShoot() 
    {
        aiming = true;
        yield return new WaitForSeconds(delayBeforeShoot);
        rigidbody2D.simulated = true;
        
        rigidbody2D.AddForce(berserk.transform.position - transform.position, ForceMode2D.Impulse);
        shooted = true;
        ready = true;
        interactable = true;
        aiming = false;
        
    }

    IEnumerator fixAnim()
    {
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.enabled = true;
    }


    //IEnumerator disable()
    //{
    //    yield return new WaitForSeconds(3f);
    //    transform.parent = null;
    //    ready = true;
    //    gameObject.SetActive(false);
    //}
}
