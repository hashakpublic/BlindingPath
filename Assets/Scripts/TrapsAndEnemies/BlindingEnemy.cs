﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindingEnemy : MonoBehaviour
{
    ActivateFlash activateFlash;

    private void Start()
    {
        activateFlash = FindObjectOfType<ActivateFlash>();
    }

    public void StartFlash() 
    {
        activateFlash.Activate();
    }
}
