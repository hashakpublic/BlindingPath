﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBossFight : MonoBehaviour
{
    UIActions uIActions;
    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        uIActions = FindObjectOfType<UIActions>();
    }

    public void StartBossFFight() 
    {
        uIActions.StartBossFight();
        uIActions.startBossFight = this;
    }

    public void startDeathAnimation() 
    {
        animator.SetTrigger("Die");
    }
}
