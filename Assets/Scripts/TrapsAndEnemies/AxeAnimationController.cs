﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeAnimationController : MonoBehaviour
{
    public bool iAmDrop = false;



    [SerializeField]GameObject DirtSplashes;
    AxeTrap axeTrap;
    public ProjectileController projectileController;
    BackGroundMovement backGroundMovement;

    Animator animator;
    Rigidbody2D rigidbody2D;
    

    bool exploded = false;
    public bool Exploded { get { return exploded; } }
    private void Start()
    {
        
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        axeTrap = GetComponent<AxeTrap>();
        
        
    }

   

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.GetComponent<Ground>())
        {
            if (iAmDrop == false)
            {
                rigidbody2D.simulated = false;
            }
            
            //rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionY| RigidbodyConstraints2D.FreezePositionX;
            //animator.SetTrigger("Explosion");
            animator.enabled = false; // удалить или нет

            exploded = true;
            transform.parent = axeTrap.backGroundMovement.transform;
            //rigidbody2D.velocity = new Vector2(axeTrap.backGroundMovement.speed, 0);
            axeTrap.interactable = false;
            //Instantiate(DirtSplashes, transform.position, Quaternion.identity, transform);
            //GameObject.Destroy(gameObject, 4f);
          
        }
        
    }



 
}
