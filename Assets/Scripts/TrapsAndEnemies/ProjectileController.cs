﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    [SerializeField]TrapsSpawn[] trapsSpawns;
    public TrapsSpawn[] TrapsSpawns { get { return trapsSpawns; } }
       
    [SerializeField]float timeBetweenSpawnTrap0Spawn = 2;
    [SerializeField] float timeBetweenSpawnTrap1Spawn = 4;
    bool trapsAreSpawning = true;

    bool inDash = false;

    public BackGroundMovement backGroundMovementLayer0;
    [SerializeField]BerserkController berserk;
    [SerializeField]BackGroundController groundController;

    public TrapParent axeTrap;
    public TrapParent boulderTrap;
    public TrapParent swordTrap;
    public TrapParent angryCloud;
    public TrapParent dropTrap;



  
    public HashSet<TrapParent> axeTrapsList;
    public HashSet<TrapParent> boulderTrapsList;
    public HashSet<TrapParent> swordsTrapsList;
    public HashSet<TrapParent> angryCloudsTrapsList;
    public HashSet<TrapParent> dropsTrapsList;
    int ezCount = 0;
    void Start()
    {
        axeTrapsList = new HashSet<TrapParent>();
        boulderTrapsList = new HashSet<TrapParent>();
        swordsTrapsList = new HashSet<TrapParent>();
        angryCloudsTrapsList = new HashSet<TrapParent>();
        dropsTrapsList = new HashSet<TrapParent>();

        foreach (TrapsSpawn trapsSpawn in trapsSpawns) 
        {
            trapsSpawn.backGroundMovement = backGroundMovementLayer0;
            trapsSpawn.berserkTransform = berserk.transform;
            trapsSpawn.projectileController = this;
        }
        //foreach (TrapParent trap in dropsTrapsList)
        //{
        //    trap.BerserksTransform = berserk.transform;
        //    trap.backGroundMovement = backGroundMovementLayer0;
        //}
        //foreach (TrapParent trap in angryCloudsTrapsList)
        //{
        //    trap.BerserksTransform = berserk.transform;
        //    trap.backGroundMovement = backGroundMovementLayer0;
        //}

        //foreach (TrapParent trap in swordsTrapsList) 
        //{
        //    trap.BerserksTransform = berserk.transform;
        //    trap.backGroundMovement = backGroundMovementLayer0;
        //}

        //foreach (TrapParent trap in axeTrapsList) 
        //{
        //    trap.backGroundMovement = backGroundMovementLayer0;
        //}
        //StartCoroutine(spawnTrap0Logic());
        //StartCoroutine(spawnTrap1Logic());
        StartCoroutine(checkTrapWorldPlaceState());
    }

 
    //Опиши тут что такое трап 1, 2, 3 и так далее
    public void SpawnTrap0() 
    {
        //foreach (TrapParent trapParent in axeTrapsList) 
        //{
        //    if (trapParent.ready) 
        //    {
        //        trapsSpawns[0].SpawnTrap(trapParent);
        //        break;
        //    }

        //}


        axeTrapsList.Add(trapsSpawns[0].SpawnTrap(axeTrap));
        
        
    }
    public void SpawnTrap1()
    {
        axeTrapsList.Add(trapsSpawns[1].SpawnTrap(axeTrap));

    }
    public void SpawnTrap2()
    {
        axeTrapsList.Add(trapsSpawns[2].SpawnTrap(axeTrap));

    }


    public void SpawnTrap3()
    {
        axeTrapsList.Add(trapsSpawns[3].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap3()
    {
        boulderTrapsList.Add(trapsSpawns[3].SpawnTrap(boulderTrap));

    }


    public void SpawnTrap4()
    {
        axeTrapsList.Add(trapsSpawns[4].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap4()
    {
        boulderTrapsList.Add(trapsSpawns[4].SpawnTrap(boulderTrap));

    }



    public void SpawnTrap5()
    {
        axeTrapsList.Add(trapsSpawns[5].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap5()
    {
        boulderTrapsList.Add(trapsSpawns[5].SpawnTrap(boulderTrap));

    }



    public void SpawnTrap6()
    {
        axeTrapsList.Add(trapsSpawns[6].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap6()
    {
        boulderTrapsList.Add(trapsSpawns[6].SpawnTrap(boulderTrap));

    }





    public void SpawnTrap7()
    {
        axeTrapsList.Add(trapsSpawns[7].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap7()
    {
        boulderTrapsList.Add(trapsSpawns[7].SpawnTrap(boulderTrap));

    }



    public void SpawnTrap8()
    {
        axeTrapsList.Add(trapsSpawns[8].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap8()
    {
        boulderTrapsList.Add(trapsSpawns[8].SpawnTrap(boulderTrap));

    }




    public void SpawnTrap9()
    {
        axeTrapsList.Add(trapsSpawns[9].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap9()
    {
        boulderTrapsList.Add(trapsSpawns[9].SpawnTrap(boulderTrap));

    }



    public void SpawnTrap10()
    {
        axeTrapsList.Add(trapsSpawns[10].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap10()
    {
        boulderTrapsList.Add(trapsSpawns[10].SpawnTrap(boulderTrap));

    }




    public void SpawnTrap11()
    {
        axeTrapsList.Add(trapsSpawns[11].SpawnTrap(axeTrap));

    }

    public void SpawnBoulderTrap11()
    {
        boulderTrapsList.Add(trapsSpawns[11].SpawnTrap(boulderTrap));

    }



    public void SpawnTrap12()
    {
        axeTrapsList.Add(trapsSpawns[12].SpawnTrap(axeTrap));

    }

    public void SpawnTrap13()
    {
        axeTrapsList.Add(trapsSpawns[13].SpawnTrap(axeTrap));

    }

    public void SpawnTrap14()
    {
        axeTrapsList.Add(trapsSpawns[14].SpawnTrap(axeTrap));

    }

    public void SpawnSwordTrap15()
    {
       swordsTrapsList.Add(trapsSpawns[15].SpawnTrap(swordTrap));

    }

    public void SpawnSwordTrap16()
    {
        swordsTrapsList.Add(trapsSpawns[16].SpawnTrap(swordTrap));

    }


    public void SpawnDropTrap(Vector2 position)
    {
        TrapParent newDropTrap = Instantiate(dropTrap, position, Quaternion.identity);
        dropsTrapsList.Add(newDropTrap);
        newDropTrap.backGroundMovement = backGroundMovementLayer0;
        newDropTrap.BerserksTransform = berserk.transform;
        newDropTrap.projectileController = this;

    }



    public void SpawnAngryCloud15()
    {
      
       angryCloudsTrapsList.Add( trapsSpawns[15].SpawnTrap(angryCloud));
        
    }

    public void SpawnAngryCloud16()
    {
        angryCloudsTrapsList.Add(trapsSpawns[16].SpawnTrap(angryCloud));

    }


    

    public void ChangeTrapsSpeed(float Multiplier, float Time)
    {

        StartCoroutine(changeTrapSpeedCoroutine(Multiplier, Time));

    }

    IEnumerator changeTrapSpeedCoroutine(float Multiplier, float Time)
    {

        foreach (TrapParent axeTrap in axeTrapsList)
        {
            if (axeTrap.ready)
            {
                if (axeTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {
                    if (axeTrap.rigidbody2D == null)
                        break;
                    
                    axeTrap.LastVelocity = axeTrap.rigidbody2D.velocity;
                    axeTrap.rigidbody2D.velocity = axeTrap.rigidbody2D.velocity - new Vector2(backGroundMovementLayer0.speed * Multiplier - 1, 0);
                    
                }
            }
        }


        foreach (TrapParent swordTrap in swordsTrapsList)
        {
            if (swordTrap.ready)
            {

                
                if (swordTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {
                    
                    if (swordTrap.rigidbody2D == null)
                        break;
                    
                    
                    swordTrap.LastVelocity = swordTrap.rigidbody2D.velocity;
                    swordTrap.rigidbody2D.velocity = swordTrap.rigidbody2D.velocity - new Vector2(backGroundMovementLayer0.speed * Multiplier - 1, 0);
                    
                }
            }
        }



        foreach (TrapParent dropTrap in dropsTrapsList)
        {
            if (dropTrap.ready)
            {


                if (dropTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {

                    if (dropTrap.rigidbody2D == null)
                        break;


                    dropTrap.LastVelocity = dropTrap.rigidbody2D.velocity;
                    dropTrap.rigidbody2D.velocity = dropTrap.rigidbody2D.velocity - new Vector2(backGroundMovementLayer0.speed * Multiplier - 1, 0);

                }
            }
        }


        foreach (TrapParent angryCloudTrap in angryCloudsTrapsList)
        {
            if (angryCloudTrap.ready)
            {


                if (angryCloudTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {

                    if (dropTrap.rigidbody2D == null)
                        break;


                    angryCloudTrap.LastVelocity = angryCloudTrap.rigidbody2D.velocity;
                    angryCloudTrap.rigidbody2D.velocity = angryCloudTrap.rigidbody2D.velocity - new Vector2(backGroundMovementLayer0.speed * Multiplier - 1, 0);

                }
            }
        }


        yield return new WaitForSeconds(Time);

        foreach (TrapParent dropTrap in dropsTrapsList)
        {
            if (dropTrap.ready)
            {


                if (dropTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {

                    if (dropTrap.rigidbody2D == null)
                        break;



                    dropTrap.rigidbody2D.velocity = dropTrap.LastVelocity;

                }
            }
        }
        foreach (TrapParent angryCloudTrap in angryCloudsTrapsList)
        {
            if (angryCloudTrap.ready)
            {


                if (angryCloudTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {

                    if (dropTrap.rigidbody2D == null)
                        break;


                    
                    angryCloudTrap.rigidbody2D.velocity = angryCloud.LastVelocity;

                }
            }
        }

        foreach (TrapParent axeTrap in axeTrapsList)
        {
            if (axeTrap.ready)
            {
                if (axeTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {
                    if (axeTrap.rigidbody2D == null)
                        break;
                    axeTrap.rigidbody2D.velocity = axeTrap.LastVelocity;
                }
            }
        }

        foreach (TrapParent swordTrap in swordsTrapsList)
        {
            if (swordTrap.ready)
            {
                if (swordTrap.interactable) //тут из за проверки булыжника могут быть проблемы с производительностью или нет
                {
                    if (swordTrap.rigidbody2D == null)
                        break;
                    swordTrap.rigidbody2D.velocity = swordTrap.LastVelocity;
                }
            }
        }

    }

    IEnumerator checkTrapWorldPlaceState()
    {
        yield return new WaitForSeconds(2);
        //foreach (TrapParent trap in axeTrapsList) 
        //{
        //    if (trap.transform.position.x < -80) 
        //    {
        //        axeTrapsList.Remove(trap);
        //        Destroy(trap);
        //    }
        //}
        //foreach (TrapParent trap in boulderTrapsList)
        //{
        //    if (trap.transform.position.x < -80)
        //    {
        //        axeTrapsList.Remove(trap);
        //        Destroy(trap);
        //    }
        //}
        //foreach (TrapParent trap in swordsTrapsList)
        //{
        //    if (trap.transform.position.x < -80)
        //    {
        //        axeTrapsList.Remove(trap);
        //        Destroy(trap);
        //    }
        //}
        //foreach (TrapParent trap in angryCloudsTrapsList)
        //{
        //    if (trap.transform.position.x < -80)
        //    {
        //        axeTrapsList.Remove(trap);
        //        Destroy(trap);
        //    }
        //}
        //foreach (TrapParent trap in dropsTrapsList)
        //{
        //    if (trap.transform.position.x < -80)
        //    {
        //        axeTrapsList.Remove(trap);
        //        Destroy(trap);
        //    }
        //}
        //StartCoroutine(checkTrapWorldPlaceState());
    }




}
