﻿
using UnityEngine;

public class BaseTrap : TrapParent
{
    public bool Fall = false;
  
    private void OnTriggerEnter2D(Collider2D collision)
    {
        BerserkController berserkController = collision.GetComponent<BerserkController>();

        if (berserkController)
        {
            if (Fall == false)
            {
                berserkController.Die();
            }
            else
            {
                berserkController.FallToDeath();
            }
        }


    }



    private void OnTriggerExit2D(Collider2D collision)
    {
        // анимация окончания неуязвимости
    }

   
    
}
