﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapParent : MonoBehaviour
{
    public string Name;
    public bool ready = true;
    public bool interactable = true;
    public Rigidbody2D rigidbody2D;
    public BackGroundMovement backGroundMovement;
    public Transform BerserksTransform;
    public ProjectileController projectileController;
    public Main Main;
    public Vector3 LastVelocity;

    protected void Start()
    {
        StartCoroutine(checkDeath());
    }

    public void AssignRigidbody() 
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        
    }
    protected IEnumerator checkDeath()
    {
        yield return new WaitForSeconds(2);
        if (transform.position.x < -40) 
        {
            
            switch (Name)
            {
                case "AxeTrapN":
                    projectileController.axeTrapsList.Remove(this);
                    break;

                case "BoulderTrapN":
                    projectileController.boulderTrapsList.Remove(this);
                    break;

                case "SwordTrapN":
                    projectileController.swordsTrapsList.Remove(this);
                    break;

                case "AngryCloudN":
                    projectileController.angryCloudsTrapsList.Remove(this);
                    break;

                case "DropTrapN":
                    projectileController.dropsTrapsList.Remove(this);

                    break;
                default:
                    print("имя ловушки не определено");
                    break;
            }
            
            Destroy(this.gameObject);
        }
        StartCoroutine(checkDeath());

    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.GetComponent<deathWall>())
    //    {

    //        print("зашел в стену стену смерти" + Name);
    //        switch (Name)
    //        {
    //            case "AxeTrapN":
    //                projectileController.axeTrapsList.Remove(this);
    //                break;

    //            case "BoulderTrapN":
    //                projectileController.boulderTrapsList.Remove(this);
    //                break;

    //            case "SwordTrapN":
    //                projectileController.swordsTrapsList.Remove(this);
    //                break;

    //            case "AngryCloudN":
    //                projectileController.angryCloudsTrapsList.Remove(this);
    //                break;

    //            case "DropTrapN":
    //                projectileController.dropsTrapsList.Remove(this);

    //                break;
    //            default:
    //                print("имя ловушки не определено");
    //                break;
    //        }
    //        print("моё имя " + Name);
    //        Destroy(this.gameObject);
            
    //    }
    //}
}
