﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BaseEnemy : MonoBehaviour
{
    [SerializeField] UnityEvent DeathEvent;

    [SerializeField] float rageCost = 10;
    public float RageCost {get{ return rageCost; }}

    [SerializeField] bool moveState = false;
    bool startMove = false;

    [SerializeField] float speed = 3;

    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
       
        if (moveState)
        {
            if (startMove)
            {
                move(speed * Time.fixedDeltaTime);
            }
            else 
            {
                if (gameObject.transform.position.x < 11)
                {
                    startMove = true;
                }
            }
        }
        
    }
    public void MeetBerserk()
    {

     DeathEvent.Invoke();
     animator.SetTrigger("Die");
     GameObject.Destroy(gameObject, 2f);
    }

    private void move(float Speed)
    {
        gameObject.transform.position = gameObject.transform.position - new Vector3(0.5f * Speed, 0, 0);
    }


}
