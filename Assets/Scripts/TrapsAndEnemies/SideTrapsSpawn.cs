﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideTrapsSpawn : TrapsSpawn
{
    

    [SerializeField] float force;

    public override TrapParent SpawnTrap(TrapParent trap) 
    {
        TrapParent newTrap = base.SpawnTrap(trap);
        newTrap.GetComponent<Rigidbody2D>().AddForce(new Vector2(force, 0), ForceMode2D.Impulse);
        return newTrap;
        
    }
}
