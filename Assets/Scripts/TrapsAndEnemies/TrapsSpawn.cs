﻿using UnityEngine;

public class TrapsSpawn : MonoBehaviour
{
    public BackGroundMovement backGroundMovement;
    public Transform berserkTransform;
    public ProjectileController projectileController;
    public Main Main;
     public virtual TrapParent SpawnTrap(TrapParent trap)
    {
        TrapParent newTrap = Instantiate(trap, transform.position, Quaternion.identity);
        newTrap.backGroundMovement = backGroundMovement;
        newTrap.BerserksTransform = berserkTransform;
        newTrap.projectileController = projectileController;
        newTrap.Main = Main;
        return newTrap;
               
    }

}
