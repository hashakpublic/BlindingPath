﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeTrap : TrapParent
{
    AxeAnimationController axeAnimationController;
    void Start()
    {
        base.Start();
        axeAnimationController = GetComponent<AxeAnimationController>();
        AssignRigidbody();
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (axeAnimationController)
        {
            if (!axeAnimationController.Exploded)
            {
                BerserkController berserkController = collision.GetComponent<BerserkController>();

                if (berserkController)
                {
                    berserkController.Die();
                }

            }
        }
    }



    //IEnumerator checkDeath()
    //{
    //    yield return new WaitForSeconds(2);
    //    if (transform.position.x < -40)
    //    {
    //        print("проверяю позицию");
    //        switch (Name)
    //        {
    //            case "AxeTrapN":
    //                projectileController.axeTrapsList.Remove(this);
    //                break;

    //            case "BoulderTrapN":
    //                projectileController.boulderTrapsList.Remove(this);
    //                break;

    //            case "SwordTrapN":
    //                projectileController.swordsTrapsList.Remove(this);
    //                break;

    //            case "AngryCloudN":
    //                projectileController.angryCloudsTrapsList.Remove(this);
    //                break;

    //            case "DropTrapN":
    //                projectileController.dropsTrapsList.Remove(this);

    //                break;
    //            default:
    //                print("имя ловушки не определено");
    //                break;
    //        }
    //        print("моё имя " + Name);
    //        Destroy(this.gameObject);
    //    }
    //    StartCoroutine(checkDeath());

    //}

}
