﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngryCloud : TrapParent
{
    TrapsSpawn trapsSpawn;
    

    SpriteRenderer spriteRenderer; // пока для эффекта


    float oneHandedthOfColor = 0;
    float currentColorState = 0;
    bool changeColorActive = false;


    [SerializeField] Color SpawningColor;
    [SerializeField] Color betweenSpawn;

    [SerializeField] float speed = 4;
    [SerializeField] float timeBetweenSpawns = 3;
    [SerializeField] float spawnsBeforeDestroy = 3;
    float spawnsPast = 0;

    bool spawning = false;
    
    void Start()
    {
        
        base.Start();
        AssignRigidbody();
        trapsSpawn = GetComponent<TrapsSpawn>();
        trapsSpawn.Main = Main;
        trapsSpawn.backGroundMovement = backGroundMovement;
        trapsSpawn.projectileController = projectileController;
        trapsSpawn.berserkTransform = BerserksTransform;
        spriteRenderer = GetComponent<SpriteRenderer>();
              
    }


    private void FixedUpdate()
    {
        SearchFoePlayer();
    }


    void SearchFoePlayer() //тут нужно переписать с учётом физики
    {
        if (transform.position.x < BerserksTransform.position.x+0.8)    //диапазон над игроком, когда нужно спавнить
        {
            if (spawning == false)
            {

                rigidbody2D.MovePosition(new Vector2(0.5f * speed * Time.fixedDeltaTime + transform.position.x,0 + transform.position.y));
            }
        }
        else
        {
            if (transform.position.x > BerserksTransform.position.x+1)
            {
                if (spawning == false)
                {
                    transform.position = transform.position - new Vector3(0.5f * speed * Time.fixedDeltaTime, 0, 0);
                }
            }
            else
            {
                SpawnProjectile();
            }

            
        }
    }

    void SpawnProjectile() 
    {
        if (spawning == false) 
        {
            StartCoroutine(spawnCoroutine());
        }
    }

    IEnumerator spawnCoroutine() 
    {
        if (changeColorActive == false)
        {
            changeColorActive = true;
            StartCoroutine(changeColor(6, spriteRenderer.color, SpawningColor));
        }
        spawning = true;
        projectileController.SpawnDropTrap(transform.position);
        spawnsPast = spawnsPast + 1;
        if (spawnsPast == spawnsBeforeDestroy) 
        {
            transform.parent = backGroundMovement.transform;
            rigidbody2D.simulated = false;
        }
        yield return new WaitForSeconds(timeBetweenSpawns);
        spawning = false;

        //if (changeColorActive == false)
        //{
        //    changeColorActive = true;
        //    StartCoroutine(changeColor(3, spriteRenderer.color, SpawningColor));
        //}
    }

    IEnumerator disable()
    {
        yield return new WaitForSeconds(3f);
        transform.parent = null;
        ready = true;
        gameObject.SetActive(false);
    }



    IEnumerator changeColor(float TimeToChange, Color toChangeFrom, Color toChangeTo) 
    {
        if (changeColorActive) 
        {
            oneHandedthOfColor = TimeToChange / 100;
            spriteRenderer.color = Color.Lerp(toChangeFrom, toChangeTo, currentColorState);
            currentColorState = currentColorState + 0.01f;

            if (currentColorState >= 1)
            {
                changeColorActive = false;
                currentColorState = 0;
            }
            else
            {
                yield return new WaitForSeconds(oneHandedthOfColor);
                StartCoroutine(changeColor(TimeToChange, toChangeFrom, toChangeTo));
            }

        }

        yield return new WaitForSeconds(0.1f);

    }
}
