﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateFlash : MonoBehaviour
{
    Image image;
    
    float aColorComponent = 1;

    private void Start()
    {
        image = GetComponent<Image>();
        
        
    }


    public void Activate() 
    {
        if (image != null)
        {

            aColorComponent = 1;
            StartCoroutine(activate());
        }
    }

    IEnumerator activate() 
    {

        image.color = new Color(0, 0, 0, aColorComponent);
        if (aColorComponent > 0)
        {
            yield return new WaitForSeconds(0.1f);
            aColorComponent = aColorComponent - 0.04f;
            StartCoroutine(activate());
        }
    }
}
