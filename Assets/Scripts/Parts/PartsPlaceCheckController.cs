﻿using UnityEngine.Events;
using UnityEngine;
using System;

public class PartsPlaceCheckController : MonoBehaviour
{
    int colliderCount = 0;
    public int ColliderCount { get { return colliderCount; } }
    public UnityAction unityAction;

    [HideInInspector]public FakeCollider[] placeChecks = new FakeCollider[44];
    [HideInInspector]public Part myParentPart;
    
    void Start()
    {
        placeChecks = GetComponentsInChildren<FakeCollider>();
        for (int i = 0; i < placeChecks.Length-1; i++) 
        {
            
            
            //placeChecks[i].PlaceNumber = i;
            placeChecks[i].spawnAction += placeChecks[i + 1].Activate;
            placeChecks[i].spawnAction += colliderChanged;
        }

        placeChecks[placeChecks.Length - 1].spawnAction += partEnds;
        

    }


    public void colliderChanged() 
    {
        colliderCount++;

    }

    public void partEnds() 
    {
        colliderCount++;
    }

   
}
