﻿
using UnityEngine;
using UnityEngine.Events;

public class FakeCollider : MonoBehaviour
{
    //public int PlaceNumber;
    public bool playerIsIn = false;
    UnityEvent SpawnTraps;
    public UnityAction spawnAction;

    private void Start()
    {
        SpawnTraps = new UnityEvent();
    }
    private void Update()
    {
        checkPisition();
        
    }
    
    void checkPisition() 
    {
        if (playerIsIn)
        {
            if (transform.position.x < -1.25f)
            {
                SpawnTraps.Invoke();
                playerIsIn = false;
            }
        }
    }

    public void Activate() 
    {
        playerIsIn = true;
        SpawnTraps.AddListener(spawnAction);
    }
}
