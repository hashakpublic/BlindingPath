﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : PartParent
{
    //всего 44 коллайдера, от 0 до 43
    [HideInInspector]public PartsController partsController;
    PartsPlaceCheckController checkController;
    public PartsPlaceCheckController CheckController { get { return checkController; } }
    [HideInInspector]public ProjectileController ProjectileController;
    bool spawnUsed = false;

    [Header("Топоры. 3-11 вертикальные спавнеры, остальные боковые")]
    [SerializeField] int[] spawnPartNummberTrapSpawn0;
    [SerializeField] int[] spawnPartNummberTrapSpawn1;
    [SerializeField] int[] spawnPartNummberTrapSpawn2;
    [SerializeField] int[] spawnPartNummberTrapSpawn3;
    [SerializeField] int[] spawnPartNummberTrapSpawn4;
    [SerializeField] int[] spawnPartNummberTrapSpawn5;
    [SerializeField] int[] spawnPartNummberTrapSpawn6;
    [SerializeField] int[] spawnPartNummberTrapSpawn7;
    [SerializeField] int[] spawnPartNummberTrapSpawn8;
    [SerializeField] int[] spawnPartNummberTrapSpawn9;
    [SerializeField] int[] spawnPartNummberTrapSpawn10;
    [SerializeField] int[] spawnPartNummberTrapSpawn11;
    [SerializeField] int[] spawnPartNummberTrapSpawn12;
    [SerializeField] int[] spawnPartNummberTrapSpawn13;
    [SerializeField] int[] spawnPartNummberTrapSpawn14;
    
    [Header("15 и 16 спавнят мечи. 15 - левый спавнер")] 
    [SerializeField] int[] spawnPartNummberTrapSpawn15;
    [SerializeField] int[] spawnPartNummberTrapSpawn16;
    
    [Header("15 и 16 спавнят облачка. 15 - левый спавнер")]
    [SerializeField] int[] spawnPartNummber_Cloud_Spawn15;
    [SerializeField] int[] spawnPartNummber_Cloud_Spawn16;
    
    [Header("Эти булыжники спавнят")]
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn3;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn4;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn5;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn6;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn7;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn8;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn9;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn10;
    [SerializeField] int[] spawnPartNummber_Boulder_Spawn11;
    

    private void Start()  // тут нужно как-то проставить ловушки для спавна
    {
        checkController = GetComponentInChildren<PartsPlaceCheckController>();
        checkController.myParentPart = this;
        StartCoroutine(setSpawners());

    }

    private void FixedUpdate()
    {
        DestroyYourself();
        timeToSpawnNext();
    }


    void timeToSpawnNext() 
    {
        if (!spawnUsed) 
        {
            if (endPoint.position.x < 20)
            {
                partsController.timeTospawn = true;
                spawnUsed = true;


                
            }
        }
    }


    IEnumerator setSpawners()  // тут проблема
    {
        yield return null;
        checkController.placeChecks[0].Activate();
        foreach (int x in spawnPartNummberTrapSpawn0)
        {
            print(checkController.placeChecks[0].spawnAction + " спавн экшн");
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap0;
        }
        foreach (int x in spawnPartNummberTrapSpawn1)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap1;
        }
        foreach (int x in spawnPartNummberTrapSpawn2)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap2;
        }
        foreach (int x in spawnPartNummberTrapSpawn3)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap3;
        }
        foreach (int x in spawnPartNummberTrapSpawn4)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap4;
        }
        foreach (int x in spawnPartNummberTrapSpawn5)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap5;
        }
        foreach (int x in spawnPartNummberTrapSpawn6)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap6;
        }
        foreach (int x in spawnPartNummberTrapSpawn7)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap7;
        }
        foreach (int x in spawnPartNummberTrapSpawn8)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap8;
        }
        foreach (int x in spawnPartNummberTrapSpawn9)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap9;
        }
        foreach (int x in spawnPartNummberTrapSpawn10)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap10;
        }
        foreach (int x in spawnPartNummberTrapSpawn11)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap11;
        }
        foreach (int x in spawnPartNummberTrapSpawn12)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap12;
        }
        foreach (int x in spawnPartNummberTrapSpawn13)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap13;
        }
        foreach (int x in spawnPartNummberTrapSpawn14)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnTrap14;
        }
        foreach (int x in spawnPartNummberTrapSpawn15)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnSwordTrap15;
        }
        foreach (int x in spawnPartNummberTrapSpawn16)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnSwordTrap16;
        }
        foreach (int x in spawnPartNummber_Cloud_Spawn15)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnAngryCloud15;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn3)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap3;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn4)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap4;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn5)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap5;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn6)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap6;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn7)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap7;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn8)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap8;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn9)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap9;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn10)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap10;
        }
        foreach (int x in spawnPartNummber_Boulder_Spawn11)
        {
            checkController.placeChecks[x].spawnAction += ProjectileController.SpawnBoulderTrap11;
        }

    }

}
