﻿using UnityEngine;

public class PartParent : MonoBehaviour
{
    [SerializeField]protected Transform endPoint;
    public Transform EndPoint { get { return endPoint; } }

    public bool marked = false;
    protected virtual void DestroyYourself() 
    {
        if (endPoint.transform.position.x < -12) 
        {
            GameObject.Destroy(gameObject);
        }
    }

    private void Start()
    {
        
    }
    private void FixedUpdate()
    {
        DestroyYourself();
    }
}
