﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree;
using System.Linq;

public class PartsController : MonoBehaviour
{
    [HideInInspector]public bool timeTospawn = false;
    [HideInInspector]public Main main;

    DinamicBachgroundMovementAccelerator movementAccelerator;

    [SerializeField] Part[] partsComp0;
    [SerializeField] Part[] partsComp1;
    [SerializeField] Part[] partsComp2;
    [SerializeField] Part[] partsComp3;
    [SerializeField] Part[] partsComp4;
    [SerializeField] Part[] partsComp5;
    [SerializeField] Part[] partsComp6;
    [SerializeField] Part[] partsComp7;
    

    HashSet<Part> parts;
    HashSet<Part> spawnedParts;
    Transform endPointPosition;

    float allScore = 0;
    string allScoreString = "allScore";

    

    void spawnAlgorithm() // возможно порядок такой себе, нужно тестить
    {
        if (timeTospawn) 
        {
            timeTospawn = false;
            if (parts.Count > 0)
            {
                Part part = Instantiate(parts.First<Part>(), endPointPosition.position, Quaternion.identity, transform);
                endPointPosition = part.EndPoint;
                part.partsController = this;
                part.ProjectileController = main.projectileController;
                spawnedParts.Add(part);
                parts.Remove(part);
            }
            else
            {
                Part part = Instantiate(spawnedParts.First<Part>(), endPointPosition.position, Quaternion.identity, transform);
                endPointPosition = part.EndPoint;
                part.partsController = this;
                part.ProjectileController = main.projectileController;
                parts.Add(part);
                spawnedParts.Remove(part);
            }

            
        }
       
    }
    private void Start()
    {
        movementAccelerator = FindObjectOfType<DinamicBachgroundMovementAccelerator>();
        spawnedParts = new HashSet<Part>();
        setParts();
        Part part = Instantiate(parts.First<Part>().gameObject, transform.position, Quaternion.identity, transform).GetComponent<Part>();
        endPointPosition = part.EndPoint;
        part.partsController = this;
        part.ProjectileController = main.projectileController;
        spawnedParts.Add(part);
        parts.Remove(part);
    }
    private void FixedUpdate()
    {
        spawnAlgorithm();
    }
    void setParts() 
    {
        parts = new HashSet<Part>();
        foreach (Part a in partsComp0) 
        {
            parts.Add(a);
        }
        switch (CheckDificaltyLvl()) 
        {
            case 1:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                break;
            case 2:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp2)
                {
                    parts.Add(a);
                }
                break;
            case 3:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp2)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp3)
                {
                    parts.Add(a);
                }
                break;
            case 4:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp2)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp3)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp4)
                {
                    parts.Add(a);
                }
                break;
            case 5:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp2)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp3)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp4)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp5)
                {
                    parts.Add(a);
                }
                break;
            case 6:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp2)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp3)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp4)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp5)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp6)
                {
                    parts.Add(a);
                }
                break;
            case 7:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp2)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp3)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp4)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp5)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp6)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp7)
                {
                    parts.Add(a);
                }
                break;
            case 8:
                foreach (Part a in partsComp1)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp2)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp3)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp4)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp5)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp6)
                {
                    parts.Add(a);
                }
                foreach (Part a in partsComp7)
                {
                    parts.Add(a);
                }
                //movementAccelerator.TurnAccelerationON();    //потооом
                
                break;
           

            default:
                Debug.Log("ошибка");
                break;
        }
    }
    public int CheckDificaltyLvl()
    {
        int avaliableLvl = 0;
        allScore = SaveGame.Load<float>(allScoreString);
        if (allScore > 60)
        {
            avaliableLvl = 1;
            if (allScore > 100)
            {
                avaliableLvl = 2;
                if (allScore > 150)
                {
                    avaliableLvl = 3;
                    if (allScore > 210)
                    {
                        avaliableLvl = 4;
                        if (allScore > 300)
                        {
                            avaliableLvl = 5;
                            if (allScore > 400)
                            {
                                avaliableLvl = 6;
                                if (allScore > 550)
                                {
                                    avaliableLvl = 7;

                                    if (allScore > 900)
                                    {
                                        avaliableLvl = 8;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return avaliableLvl;
    }
}
