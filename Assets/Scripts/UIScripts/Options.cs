﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    [SerializeField]Toggle weatherTogle;
    bool weatherEnebled;
    string enebleWeather = "enebleWeather";

    [SerializeField] Scrollbar layerScrollBar;
    [SerializeField] Text layerText;
    float layers;
    string layersNum = "layersNum";


    [SerializeField] Toggle joystickTogle;
    bool joystick;
    string joystickEnable = "joystickEnable";




    private void Awake()
    {

        layers = SaveGame.Load<float>(layersNum, 1f);
        layerScrollBar.value = layers;
        chechLayersText();


        weatherEnebled = SaveGame.Load<bool>(enebleWeather, true);
        weatherTogle.SetIsOnWithoutNotify(weatherEnebled);
     

        joystick = SaveGame.Load<bool>(joystickEnable, true);
        joystickTogle.SetIsOnWithoutNotify(joystick);

    }


    void chechLayersText() 
    {
        if (layers > 0.25)
        {
            if (layers < 0.5)
            {
                layerText.text = "Layers: 2";
            }
            else
            {
                if (layers > 0.75)
                {
                    layerText.text = "Layers: 6";
                }
                else
                {
                    layerText.text = "Layers: 4";
                }
            }
        }
        else
        {
            layerText.text = "Layers: 1";
        }
    }
    public void MoveLayerScrollBar() 
    {
        layers = layerScrollBar.value;
        SaveGame.Save<float>(layersNum, layers);
        chechLayersText();
    }

    public void SwitchWeatherState() 
    {
        if (weatherEnebled)
        {
            weatherEnebled = false;
        }
        else
        {
            weatherEnebled = true;
        }
        SaveGame.Save<bool>(enebleWeather, weatherEnebled);
    }

    public void SwitchJoystickState()
    {
        if (joystick)
        {
            joystick = false;
        }
        else
        {
            joystick = true;
        }
        SaveGame.Save<bool>(joystickEnable, joystick);
    }

}
