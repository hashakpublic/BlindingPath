﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBarController : MonoBehaviour
{
    [SerializeField]BerserkController berserk;
    RectTransform rectTransform;
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void SetHPBar() 
    {
        rectTransform.localScale = new Vector3(berserk.CurrentBerserksRage / 100, 1, 1);
    }
}
