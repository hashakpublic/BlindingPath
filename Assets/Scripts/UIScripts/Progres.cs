﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Progres : MonoBehaviour
{
    [SerializeField]Text text;
    float avaliableDifficalty;
    MainMenu mainMenu;


    [SerializeField]RectTransform tier0;
    [SerializeField] RectTransform tier0SecretPanel; //
    [SerializeField] Text tier0Txt;

    [SerializeField] RectTransform tier1;
    [SerializeField] RectTransform tier1SecretPanel;// 60
    [SerializeField] Text tier1Txt;

    [SerializeField] RectTransform tier2;
    [SerializeField] RectTransform tier2SecretPanel;// 100
    [SerializeField] Text tier2Txt;

    [SerializeField] RectTransform tier3;
    [SerializeField] RectTransform tier3SecretPanel;// 150
    [SerializeField] Text tier3Txt;

    [SerializeField] RectTransform tier4;
    [SerializeField] RectTransform tier4SecretPanel;// 210
    [SerializeField] Text tier4Txt;

    [SerializeField] RectTransform tier5;
    [SerializeField] RectTransform tier5SecretPanel;// 300
    [SerializeField] Text tier5Txt;

    [SerializeField] RectTransform tier6;
    [SerializeField] RectTransform tier6SecretPanel;// 400
    [SerializeField] Text tier6Txt;

    [SerializeField] RectTransform tier7;
    [SerializeField] RectTransform tier7SecretPanel;// 550
    [SerializeField] Text tier7Txt;

                                                    //900

    private void Start()
    {
        mainMenu = FindObjectOfType<MainMenu>();
        avaliableDifficalty = mainMenu.CheckDificaltyLvl();
        text.text = "Your experience score is " + mainMenu.AllScore;
        setStartExpStrips();
        setExpStrips();
        
    }
    void setStartExpStrips() 
    {
        tier0.localScale = new Vector2(1, 0);
        tier1.localScale = new Vector2(1, 0);
        tier2.localScale = new Vector2(1, 0);
        tier3.localScale = new Vector2(1, 0);
        tier4.localScale = new Vector2(1, 0);
        tier5.localScale = new Vector2(1, 0);
        tier6.localScale = new Vector2(1, 0);
        tier7.localScale = new Vector2(1, 0);
        

    }

    void setExpStrips() 
    {
        if (mainMenu.AllScore < 61)
        {
            tier0.localScale = new Vector2(1, mainMenu.AllScore / 100);
            tier0SecretPanel.localScale = new Vector2(1-mainMenu.AllScore / 100, 1);
            tier0Txt.text = mainMenu.AllScore + "/60";
        }
        else 
        {
            tier0.localScale = new Vector2(1, 1);
            tier0SecretPanel.localScale = new Vector2(0, 1);
            tier0Txt.text ="60/60";
            if (mainMenu.AllScore < 161)
            {
                
                tier1.localScale = new Vector2(1, (mainMenu.AllScore - 60) / 100);
                tier1SecretPanel.localScale = new Vector2(1-(mainMenu.AllScore - 60) / 100, 1);
                tier1Txt.text = (mainMenu.AllScore-60) + "/100";

            }
            else
            {
                tier1Txt.text = "100/100";
                
                tier1.localScale = new Vector2(1, 1);
                tier1SecretPanel.localScale = new Vector2(0, 1);

                if (mainMenu.AllScore < 311)
                {
                    
                    tier2.localScale = new Vector2(1, (mainMenu.AllScore - 160) / 100);
                    tier2SecretPanel.localScale = new Vector2(1-(mainMenu.AllScore - 160) / 100, 1);
                    tier2Txt.text = (mainMenu.AllScore-160) + "/150";

                }
                else
                {
                    
                    tier2.localScale = new Vector2(1, 1);
                    tier2SecretPanel.localScale = new Vector2(0, 1);
                    tier2Txt.text = "150/150";
                    if (mainMenu.AllScore < 521)
                    {
                        
                        tier3.localScale = new Vector2(1, (mainMenu.AllScore - 310) / 100);
                        tier3SecretPanel.localScale = new Vector2(1-(mainMenu.AllScore - 310) / 100, 1);
                        tier3Txt.text = (mainMenu.AllScore-310) + "/210";

                    }
                    else
                    {
                        
                        tier3.localScale = new Vector2(1, 1);
                        tier3SecretPanel.localScale = new Vector2(0, 1);
                        tier3Txt.text = "210/210";
                        if (mainMenu.AllScore < 821)
                        {
                            
                            tier4.localScale = new Vector2(1, (mainMenu.AllScore - 520) / 100);
                            tier4SecretPanel.localScale = new Vector2(1-(mainMenu.AllScore - 520) / 100, 1);
                            tier4Txt.text = (mainMenu.AllScore-520) + "/300";

                        }
                        else
                        {
                            
                            tier4.localScale = new Vector2(1, 1);
                            tier4SecretPanel.localScale = new Vector2(0, 1);
                            tier4Txt.text = "300/300";
                            if (mainMenu.AllScore < 1221)
                            {
                                
                                tier5.localScale = new Vector2(1, (mainMenu.AllScore - 820) / 100);
                                tier5SecretPanel.localScale = new Vector2(1-(mainMenu.AllScore - 820) / 100, 1);
                                tier5Txt.text = mainMenu.AllScore-820 + "/400";

                            }
                            else
                            {
                                
                                tier5.localScale = new Vector2(1, 1);
                                tier5SecretPanel.localScale = new Vector2(0, 1);
                                tier5Txt.text = "400/400";
                                if (mainMenu.AllScore < 1771)
                                {
                                    
                                    tier6.localScale = new Vector2(1, (mainMenu.AllScore - 1220) / 100);
                                    tier6SecretPanel.localScale = new Vector2(1-(mainMenu.AllScore - 1220) / 100, 1);
                                    tier6Txt.text = mainMenu.AllScore-1220 + "/550";

                                }
                                else
                                {
                                    
                                    tier6.localScale = new Vector2(1, 1);
                                    tier6SecretPanel.localScale = new Vector2(0, 1);
                                    tier6Txt.text = "550/550";
                                    if (mainMenu.AllScore < 2671)
                                    {
                                        
                                        tier7.localScale = new Vector2(1, (mainMenu.AllScore - 1770) / 100);
                                        tier7SecretPanel.localScale = new Vector2(1-(mainMenu.AllScore - 1770) / 100, 1);
                                        tier7Txt.text = (mainMenu.AllScore-1770) + "/900";

                                    }
                                    else
                                    {
                                        
                                        tier7.localScale = new Vector2(1, 1);
                                        tier7SecretPanel.localScale = new Vector2(0, 1);
                                        tier7Txt.text = "900/900";

                                    }

                                }

                            }

                        }

                    }

                }

            }
        }
    }
}
