﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bestiary : MonoBehaviour
{
    [SerializeField]ScrollRect BestiaryScrollRect;

    [SerializeField]RectTransform Bereza;
    [SerializeField]RectTransform Imp;


    void Start()
    {

        

        Bereza.gameObject.SetActive(true);
        BestiaryScrollRect.content = Bereza;
        


    }

    public void ShowBereza() 
    {
        BestiaryScrollRect.content.gameObject.SetActive(false);
        Bereza.gameObject.SetActive(true);
        BestiaryScrollRect.content = Bereza;
    }

    public void ShowImp() 
    {
        BestiaryScrollRect.content.gameObject.SetActive(false);
        Imp.gameObject.SetActive(true);
        BestiaryScrollRect.content = Imp;
    }

    
}
