﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIActions : MonoBehaviour
{

    [SerializeField] BossFight BossFight;
    [SerializeField] GameObject Scene;
    Canvas canvas;

    [SerializeField]GameObject GameOverPanel;
    [SerializeField] Text gameOverScore;
    [SerializeField] Text gameOverMaxScore;
    [SerializeField] ScoreScript scoreScript;
    public StartBossFight startBossFight;

    


    private void Start()
    {

        canvas = FindObjectOfType<Canvas>();
    }
    public void Restart() 
    {
     SceneManager.LoadScene("GamePlay", LoadSceneMode.Single);
        Time.timeScale = 1;
    }


    public void GameOver() 
    {
        GameOverPanel.SetActive(true);
        StartCoroutine(waitForDeathAnim());
    }

    public void Exit() 
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        Time.timeScale = 1;
    }

    public void ShowGameOverScore() 
    {
        if (scoreScript != null) 
        {
            scoreScript.CheckMaxScore();
            gameOverScore.text = "Final Score: " + scoreScript.CurrentScore;
            gameOverMaxScore.text = "BestScore: " + scoreScript.MaxScore;
            scoreScript.SaveScores();
        }
    }

    public void StartBossFight() 
    {
        BossFight bossFight =  Instantiate(BossFight, canvas.transform);
        
    }
    public IEnumerator waitForDeathAnim() 
    {

    
        yield return new WaitForSecondsRealtime(0.5f);
        Time.timeScale = 0;
      
        
    }

    public void playBossDeathAnimayion() 
    {
        startBossFight.startDeathAnimation(); 
    }

    public IEnumerator normalaiseTimeCor() 
    {
       yield return new WaitForSecondsRealtime(0.1f);
        Time.timeScale = Time.timeScale + 0.1f;
        if (Time.timeScale < 1) 
        {
            StartCoroutine(normalaiseTimeCor());
        }
        

    }
}
