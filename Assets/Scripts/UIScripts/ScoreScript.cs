﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGameFree;


public class ScoreScript : MonoBehaviour
{

    // доделать систему 



    Text text;
    float currentScore = 0;
    

    float allScore = 0;
    float maxScore = 0;
    

    string allScoreString = "allScore";
    string maxScoreString = "maxScore";
    
    public float MaxScore { get { return maxScore; } }
    public float AllScore { get { return allScore; } }

    



    bool isActive = true;

    public float CurrentScore { get { return currentScore; } }

    private void Start()
    {
        text = GetComponent<Text>();
        AddScore(0);
        StartCoroutine(scoreCoroutine());

       

        allScore = SaveGame.Load<float>(allScoreString, 0f);
        maxScore = SaveGame.Load<float>(maxScoreString, 0f);

       
    }

    public void AddScore(float newScore) 
    {
        currentScore = currentScore + newScore;
        allScore = allScore + newScore;
        text.text = "Score: " + currentScore;

    }


    public void CheckMaxScore()
    {
        if (currentScore > maxScore)
        {

            maxScore = currentScore;
        }

    }

    public void SaveScores() 
    {


        SaveGame.Save<float>(allScoreString, allScore);
        SaveGame.Save<float>(maxScoreString, maxScore);
    }
    

    public void TurnScoreOn() 
    {
        isActive = true;
    }

    public void TurnScoreOff()
    {
        isActive = false;
    }

    IEnumerator scoreCoroutine() 
    {
        yield return new WaitForSeconds(1);
        if (isActive)
        {
            AddScore(1);
            StartCoroutine(scoreCoroutine());
        }
    }
}
