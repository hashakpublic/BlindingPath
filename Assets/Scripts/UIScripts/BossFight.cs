﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class BossFight : MonoBehaviour
{


    UIActions uIActions;
    
    
    [SerializeField] float multyHitButtHits = 4;
    float currentClicks = 0;

    [SerializeField] Button[] buttons;
    [SerializeField] Text[] multyButtonTexts;

    int buttonNumber = 0;
   

    [SerializeField] Text timeText;

    [SerializeField] float TimeForFight = 3;
    float currentTimeForFight;
    void Start()
    {
        Time.timeScale = 0f;
        currentTimeForFight = TimeForFight;
        setTime();
        StartCoroutine(bossFightCoroutine());
        uIActions = FindObjectOfType<UIActions>();

        foreach (Text text in multyButtonTexts) 
        {
            text.text = multyHitButtHits + " Hits left";
        }
        

    }

    
    void setTime() 
    {
        timeText.text = "Time left: " + currentTimeForFight;
    }

    public void SingleClickButt() 
    {

        buttonNumber = buttonNumber + 1;


        if (buttonNumber == buttons.Length)
        {
            Win();
        }
        else 
        {
            buttons[buttonNumber - 1].gameObject.SetActive(false);
            buttons[buttonNumber].gameObject.SetActive(true);

        }
    }

    public void MultyClickButt(Text multyButtonText) 
    {
        currentClicks = currentClicks + 1;

        multyButtonText.text = (multyHitButtHits-currentClicks) + " Hits left";


        if (currentClicks == multyHitButtHits) 
        {
            buttonNumber = buttonNumber + 1;

            if (buttonNumber == buttons.Length)
            {
                Win();
            }
            else
            {
                buttons[buttonNumber - 1].gameObject.SetActive(false);
                buttons[buttonNumber].gameObject.SetActive(true);
                currentClicks = 0;

            }

        }
    }
    
    public void Win() 
    {
        gameObject.SetActive(false);
        
        //Scene.SetActive(true);
        Time.timeScale = 0.6f;
        uIActions.StartCoroutine(uIActions.normalaiseTimeCor());
        uIActions.startBossFight.startDeathAnimation();
        GameObject.Destroy(gameObject);


    }

    public void Defeat() 
    {
        
        StartCoroutine(defeat());

        
    }

    IEnumerator defeat() 
    {

        yield return new WaitForSecondsRealtime(0.1f);
        
        uIActions.GameOver();
        uIActions.ShowGameOverScore();

        GameObject.Destroy(gameObject);

    }
    IEnumerator bossFightCoroutine()
    {
        yield return new WaitForSecondsRealtime(1);
        currentTimeForFight = currentTimeForFight - 1;
        if (currentTimeForFight < 0)
        {

            Defeat();
        }
        else 
        {
            setTime();
            StartCoroutine(bossFightCoroutine());
        }

    }

   
}
