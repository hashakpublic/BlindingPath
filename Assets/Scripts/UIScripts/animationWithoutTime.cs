using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class animationWithoutTime : MonoBehaviour
{
    public Sprite[] animationSprites;
    public Image imageToAnimate;
    public float pauseBetweenSprites;
    public bool inUpdate = false;
    float spriteVariable = 0;
    int spriteNumber = 0;
    



    private void Start()
    {
        imageToAnimate.sprite = animationSprites[spriteNumber];
        if (inUpdate == false)
        {
            StartCoroutine(playAnimation());
        }
    }


    //private void Update() // �� ��� ��� ������ �� ���������. ������ �� �� ������ ���� ������? 
    //{
    //    if (inUpdate)
    //    {
    //        spriteVariable = spriteVariable + 0.0001f;
    //        if (spriteVariable >= pauseBetweenSprites)
    //        {
    //            if (spriteNumber >= animationSprites.Length)
    //            {
    //                spriteNumber = 0;
    //            }
    //            else
    //            {
    //                spriteNumber = spriteNumber + 1;
    //            }
    //            imageToAnimate.sprite = animationSprites[spriteNumber];
    //        }
    //    }
    //}



    IEnumerator playAnimation() 
    {
        yield return new WaitForSecondsRealtime(pauseBetweenSprites);
        spriteNumber = spriteNumber + 1;
        if (spriteNumber >= animationSprites.Length) 
        {
            spriteNumber = 0;
        }
        imageToAnimate.sprite = animationSprites[spriteNumber];
        
        StartCoroutine(playAnimation());
    }
}
