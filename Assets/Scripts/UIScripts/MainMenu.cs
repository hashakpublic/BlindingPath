﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using BayatGames.SaveGameFree;

public class MainMenu : MonoBehaviour
{
    [SerializeField]GameObject MainMenuM;
    [SerializeField]GameObject Options;
    [SerializeField]GameObject Bestiary;
    [SerializeField] GameObject ProgressPanel;

    float allScore = 0;
    public float AllScore { get { return allScore; } }

    string allScoreString = "allScore";


    public void Restart()
    {
        SceneManager.LoadScene("GamePlay", LoadSceneMode.Single);
        Time.timeScale = 1;
    }





    public void TurnOptionsOn() 
    {
        MainMenuM.SetActive(false);
        Options.SetActive(true);

    }

    public void TurnOptionsOff() 
    {
        MainMenuM.SetActive(true);
        Options.SetActive(false);
    }


    public void TurnBestiaryOn()
    {
        MainMenuM.SetActive(false);
        Bestiary.SetActive(true);

    }

    public void TurnBestiaryOff()
    {
        MainMenuM.SetActive(true);
        Bestiary.SetActive(false);

    }
    public void TurnProgresPanelOn()
    {
        MainMenuM.SetActive(false);
        ProgressPanel.SetActive(true);

    }

    public void TurnProgresPanelOff()
    {
        MainMenuM.SetActive(true);
        ProgressPanel.SetActive(false);

    }


    public int CheckDificaltyLvl() 
    {
        int avaliableLvl = 0;
        allScore = SaveGame.Load<float>(allScoreString);
        if (allScore > 60)
        {
            avaliableLvl = 1;
            if (allScore > 100)
            {
                avaliableLvl = 2;
                if (allScore > 150)
                {
                    avaliableLvl = 3;
                    if (allScore > 210)
                    {
                        avaliableLvl = 4;
                        if (allScore > 300)
                        {
                            avaliableLvl = 5;
                            if (allScore > 400)
                            {
                                avaliableLvl = 6;
                                if (allScore > 550)
                                {
                                    avaliableLvl = 7;

                                    if (allScore > 900)
                                    {
                                        avaliableLvl = 8;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return avaliableLvl;
    }


}
