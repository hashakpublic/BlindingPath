﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Weather : MonoBehaviour
{

    public bool Enebled = true;
    SpriteRenderer[] spriteRenderers;
    [SerializeField] float TimeForColorChange = 1;
    [SerializeField] Color[] colors;
    float t;
    bool growing = true;
    void Start()
    {
        if (Enebled)
        {
            spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            t = (Random.Range(0, 100)) * 0.01f;
            StartCoroutine(weatherCoroutine());
        }
        
    }

   
    IEnumerator weatherCoroutine() 
    {
        

            foreach (SpriteRenderer renderer in spriteRenderers)
            {
                renderer.color = Color.Lerp(colors[0], colors[1], t);

            }
            if (growing)
            {
                t = t + 0.002f;
                if (t >= 1)
                {
                    growing = false;
                }
            }
            else
            {
                t = t - 0.004f;
                if (t <= 0) 
                {
                    growing = true;
                }
            }
            
        
        yield return new WaitForSeconds(0.2f);
        StartCoroutine(weatherCoroutine());
    }
}
