﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree;

public class BackGroundOptions : MonoBehaviour
{
    float layers;
    string layersNum = "layersNum";


    [SerializeField]GameObject layer2;
    [SerializeField] GameObject backGroundSquare;


    [SerializeField] GameObject layer3;
    [SerializeField] GameObject layer4;
    [SerializeField] GameObject layer5;
    [SerializeField] GameObject layer6;


    private void Start()
    {
        layers = SaveGame.Load<float>(layersNum, 1f);
        setLayers();
        
    }

    void setLayers() 
    {
        layer2.SetActive(false);
        backGroundSquare.SetActive(false);
        layer3.SetActive(false);
        layer4.SetActive(false);
        layer5.SetActive(false);
        layer6.SetActive(false);






        if (layers > 0.25) 
        {
            layer2.SetActive(true);
            backGroundSquare.SetActive(true);

            if (layers > 0.5) 
            {
                layer3.SetActive(true);
                layer4.SetActive(true);
                if (layers > 0.75) 
                {
                    layer5.SetActive(true);
                    layer6.SetActive(true);
                }
            }
        }
    }

}
