﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundPartController : MonoBehaviour
{
    [SerializeField] PartParent[] partParents;
    Transform currentPartTransform;
    int partNumberCount = 0;
    
    private void Start()
    {
        

       currentPartTransform = Instantiate(partParents[partNumberCount], transform).EndPoint.transform;
        partNumberCount++;
      
    }


    private void FixedUpdate()
    {
        SpawnNextPart();
    }

    void SpawnNextPart()
    {
        if (currentPartTransform.position.x < 11)
        {
            currentPartTransform = Instantiate(partParents[partNumberCount], currentPartTransform.position, Quaternion.identity, transform).EndPoint.transform;
            partNumberCount++;

            if (partNumberCount >= partParents.Length)
            {
                partNumberCount = 0;
            }

        }
    }

}
