﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMovement : MonoBehaviour
{
    [SerializeField]public float speed = 5;
    float startSpeed;
    Rigidbody2D rigidbody2D;


    private void Start()
    {
        speed = speed * 1;
        startSpeed = speed;
        rigidbody2D = GetComponent<Rigidbody2D>();
        SetStandartSpeed();
        
    }

    private void Update()
    {
        transform.position = new Vector2(transform.position.x - speed *Time.deltaTime, transform.position.y);
    }



    public void changeSpeed(float multiplier)
    {
        speed = speed*multiplier;
    }

    public void SetStandartSpeed() 
    {
        speed = startSpeed;
    }
}
