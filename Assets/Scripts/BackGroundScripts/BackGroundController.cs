﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundController : MonoBehaviour
{
    BackGroundMovement[] backGroundMovements;
       
    private void Start()
    {
        backGroundMovements = GetComponentsInChildren<BackGroundMovement>();
        StartCoroutine(boostSpeedCoroutine(1, 0.1f));
    }
 

    


    IEnumerator boostSpeedCoroutine(float multiplier, float time)
    {
        foreach (BackGroundMovement member in backGroundMovements)
        {
            member.changeSpeed(multiplier);

        }
        yield return new WaitForSeconds(time);
        foreach (BackGroundMovement member in backGroundMovements)
        {
            member.SetStandartSpeed();

        }
    }
    public void BoostSpeed(float multiplier, float time)
    {
        StartCoroutine(boostSpeedCoroutine(multiplier, time));
               
    }

}
