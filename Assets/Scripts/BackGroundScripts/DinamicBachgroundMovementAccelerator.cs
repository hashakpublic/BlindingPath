﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinamicBachgroundMovementAccelerator : MonoBehaviour
{
    BackGroundMovement[] backGroundMovements;

    [SerializeField] float acceleratorMultiplier = 1.2f;
    [SerializeField] float timeToNextAcceleration = 10;

    [SerializeField] bool accelerationON = false;

    private void Start()
    {
        backGroundMovements = GetComponentsInChildren<BackGroundMovement>();
        StartCoroutine(AcceleratorCoroutine());


        Debug.Log("собрано " + backGroundMovements.Length + "бэкраундмувментов");
    }

    IEnumerator AcceleratorCoroutine() 
    {
        if (accelerationON)
        {
            yield return new WaitForSeconds(timeToNextAcceleration);
            foreach (BackGroundMovement movement in backGroundMovements)
            {
                movement.speed = movement.speed * acceleratorMultiplier;
            }
            StartCoroutine(AcceleratorCoroutine());
        }
    }

    public void TurnAccelerationON() 
    {
        if (!accelerationON)
        {
            accelerationON = true;
            StartCoroutine(AcceleratorCoroutine());
        }
        else 
        {
            Debug.Log("цскорение выключено");
        }
    }

    public void TurnAccelerationOff()
    {
        accelerationON = false ;
    }
}
