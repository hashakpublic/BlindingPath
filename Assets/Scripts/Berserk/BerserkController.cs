﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Lean.Gui;

public class BerserkController : MonoBehaviour
{
    public float MaxBerserksRage { get; private set; } = 100;
    public float CurrentBerserksRage { get { return currentBerserksRage; } }
    float currentBerserksRage = 100;

    [SerializeField] float dashRageCost = 10;
    public ProjectileController projectileController;
    public float DashRageCost { get { return dashRageCost; }  }

    [SerializeField]float rageLossInSeconds = 5;
    public float RageLossInSeconds { get { return rageLossInSeconds; }}

    CharacterController2D characterController;
        BerserkAnimationController berserkAnimationController;

    float startingDashTime = 0;
    float dashTimeGone = 0;
    
         bool isDead = false;
        [SerializeField]UnityEvent DeathEvent;
        [SerializeField] UnityEvent GetRageEvent;
        [SerializeField] UnityEvent DashEvent;
    [HideInInspector] public float SpeedBoostMultiplier;

    bool inDash = false;
        public bool InDash {get { return inDash; }}


        [SerializeField] float dashDuration = 0.1f; 


        public float DashDuration { get { return dashDuration; } }


        float horizontalMove = 0;

        LeanJoystick leanJoystick;

        [SerializeField] float moveSpeed = 40;

        // Start is called before the first frame update
        void Start()
        {
            leanJoystick = FindObjectOfType<LeanJoystick>();
            berserkAnimationController = GetComponent<BerserkAnimationController>();
            characterController = GetComponent<CharacterController2D>();
            StartCoroutine(loseRage());
        }
            
     
    
    private void FixedUpdate()
    {
        characterController.Move(horizontalMove*Time.fixedDeltaTime);



    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        MeetEnemy(collision);
    }

    public void ChangeHorizontalMoveByTouch() 
    {

        if (leanJoystick.ScaledValue.x > 0.05)
        {
            horizontalMove = moveSpeed;
        }
        else
        {
            if (leanJoystick.ScaledValue.x < -0.05)
            {
                horizontalMove = -moveSpeed;
            }
            else 
            {
                horizontalMove = 0;
            }
        }

        
        
    }
    void MeetEnemy(Collider2D collider) 
    {
        BaseEnemy enemy = collider.GetComponent<BaseEnemy>(); //тут можно сделать общий класс противников
        if (enemy) 
        {
            berserkAnimationController.Attack();
            enemy.MeetBerserk();
            GetBerserksRage(enemy.RageCost);
        }
    }

    public void Die() 
    {
        
        if (!inDash)
        {
            DeathEvent.Invoke();
            isDead = true;
            berserkAnimationController.Die();
        }


    }


    public void FallToDeath()
    {
        
        if (!inDash)
        {
            DeathEvent.Invoke();
            isDead = true;
            berserkAnimationController.Die();
        }
       

    }
    public void Dash() 
    {
        if (!inDash&&(currentBerserksRage>dashRageCost)) 
        {
            StartCoroutine(dashCoroutine());
            
        }
    }

    IEnumerator dashCoroutine() 
    {
        inDash = true;
        GetBerserksRage(-dashRageCost);
        berserkAnimationController.Jump(true);
        DashEvent.Invoke();
        projectileController.ChangeTrapsSpeed(SpeedBoostMultiplier, dashDuration);
        yield return new WaitForSeconds(dashDuration);
        inDash = false;
        berserkAnimationController.Jump(false);
    }
    public void GetBerserksRage(float value) 
    {
        currentBerserksRage = CurrentBerserksRage + value;
        if (currentBerserksRage > 100) 
        {
            currentBerserksRage = 100;
        }
        GetRageEvent.Invoke();
        if (currentBerserksRage <= 0)
        {
            DeathEvent.Invoke();
        }

    }


    IEnumerator loseRage() 
    {
        yield  return new WaitForSeconds(0.5f);
        GetBerserksRage(-rageLossInSeconds / 2);
        if (currentBerserksRage > 0) 
        {
            StartCoroutine(loseRage());
        }
       
    }
  
}

