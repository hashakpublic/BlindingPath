﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerserkAnimationController : MonoBehaviour
{
    Animator berserksAnimator;


    void Start()
    {
        berserksAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Die()
    {
        berserksAnimator.SetTrigger("Hurt");
    }

    public void Fall()
    {
        berserksAnimator.SetTrigger("Fall");

    }

    public void Attack()
    {
        berserksAnimator.SetTrigger("Attack");
    }

    public void Jump(bool state)
    {
        berserksAnimator.SetBool("dash", state);
       
    }

}
