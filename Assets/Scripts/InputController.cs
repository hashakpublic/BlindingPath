﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] BackGroundController groundController;
    
    
    [SerializeField] BerserkController berserkController;

    [SerializeField] float SpeedBoostMultiplier = 2;
    private void Start()
    {
        berserkController.projectileController = GetComponent<Main>().projectileController;
        berserkController.SpeedBoostMultiplier = SpeedBoostMultiplier;
    }


    public void Dash() 
    {
        if ((!berserkController.InDash) && (berserkController.CurrentBerserksRage > berserkController.DashRageCost))
        {
            berserkController.Dash();
            groundController.BoostSpeed(SpeedBoostMultiplier, berserkController.DashDuration);
        }
    }
}
