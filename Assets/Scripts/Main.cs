﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGameFree;

public class Main : MonoBehaviour
{
    [SerializeField] Weather weather;
    string enebleWeather = "enebleWeather";

    bool joystick;
    string joystickEnable = "joystickEnable";

    public ProjectileController projectileController;
    public cameraController cameraController;
    [SerializeField] GameObject Joystick;
    [SerializeField] GameObject MovingPanel;
    [SerializeField] PartsController partsController;


    private void Awake()
    {
        partsController.main = this;
        joystick = SaveGame.Load<bool>(joystickEnable, false);
        checkСontrol();

        weather.Enebled = SaveGame.Load<bool>(enebleWeather, true);
    }

    void checkСontrol() 
    {
        Joystick.SetActive(false);
        MovingPanel.SetActive(false);

        if (joystick)
        {
            Joystick.SetActive(true);
        }
        else 
        {
            MovingPanel.SetActive(true);
        }
    }
}



