﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerserkMovement : MonoBehaviour
{
    
    [SerializeField]float MaxRage = 100f;
    [SerializeField]float dashCost = 5;
    [SerializeField]float stopCost = 10;
    float currentRage = 100f;
    [SerializeField]float speed = 0.000000000000000000000000000000000000000000000000000000000000000000005f;
    [SerializeField] float dashMultiplier = 4;
    [SerializeField] float dashLength = 3;

    float timeToSingleMove = 0.03125f; // Это будет 1 раз за кадр, если у нас 32 кадра в секунду


    [SerializeField]float coroutineLegnthMultiplier = 3; // отвечает за то как часто будет изменяться скорость игрока

    Vector3 vectorSpeed;
    bool run = true;

    private void Start()
    {
        vectorSpeed = new Vector3(speed, 0, 0);
        //StartCoroutine(runCoroutine());
    }

    private void FixedUpdate()
    {
        Run();
        if (Input.GetButtonDown("Jump"))
        {
            Dash();
        }
    }




    public void Dash()
    {
        StartCoroutine(dashCoroutine());
    }

    public void Run()
    {
        gameObject.transform.position = gameObject.transform.position + vectorSpeed;
    }

    public void Stop()
    {

    }
    IEnumerator runCoroutine()
    {
        Run();
        yield return new WaitForSeconds(timeToSingleMove);
        
        StartCoroutine(runCoroutine());
    }
    IEnumerator dashCoroutine()
    {
        float oldSpeed = vectorSpeed.x;
               
        float newSpeed = vectorSpeed.x*dashMultiplier;

        


         for (float x = 0; x >= (dashLength*coroutineLegnthMultiplier); x++)
        {
            yield return new WaitForSeconds((dashLength/dashMultiplier)/coroutineLegnthMultiplier);
            vectorSpeed = vectorSpeed - new Vector3((oldSpeed * (dashMultiplier - 1)/ dashLength), 0, 0);
        }
        Debug.Log(vectorSpeed.x);
        Debug.Log(oldSpeed);

    }
}
